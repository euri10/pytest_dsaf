# noqa: D100
import pytest

DOCKER_COMPOSE = """version: '3.9'
services:
  db:
    image: postgres
    environment:
      POSTGRES_PASSWORD: example_password
    ports:
      - "5555:5432"
"""


def test_compose_path(pytester: pytest.Pytester) -> None:
    """Test that the default compose file is found in the test root directory."""
    pytester.makefile(".yml", docker_compose=DOCKER_COMPOSE)
    pytester.makepyfile(
        """
        def test_it(dsaf) -> None:
            pass
    """
    )
    result = pytester.runpytest()
    result.assert_outcomes(passed=1)


def test_override_compose_path(pytester: pytest.Pytester) -> None:
    """Test that a defined compose file is found in the test root directory."""
    pytester.makefile(".yml", toto=DOCKER_COMPOSE)
    pytester.makepyfile(
        """
        from pathlib import Path
        import pytest

        @pytest.fixture(scope="session")
        def dsaf_compose_file() -> Path:
            return Path("toto.yml")

        def test_it(dsaf) -> None:
            pass
    """
    )
    result = pytester.runpytest()
    result.assert_outcomes(passed=1)


def test_compose_path_not_found(pytester: pytest.Pytester) -> None:
    """Test that a defined compose file is not found."""
    pytester.makepyfile(
        """
        from pathlib import Path
        import pytest

        @pytest.fixture(scope="session")
        def dsaf_compose_file() -> Path:
            return Path("toto.yml")

        def test_it(dsaf) -> None:
            pass
    """
    )
    result = pytester.runpytest()
    result.assert_outcomes(errors=1)
    result.stdout.fnmatch_lines(["*File not found: *toto.yml"])


@pytest.mark.parametrize("scope", ["session", "module", "class", "function"])
def test_fixture_scopes(pytester: pytest.Pytester, scope: str) -> None:
    """Test that the fixtures are correctly scoped."""
    pytester.makefile(".yml", docker_compose=DOCKER_COMPOSE)
    pytester.makepyfile(
        f"""
        from pathlib import Path
        import pytest

        @pytest.fixture(scope="{scope}")
        def dsaf_compose_file() -> Path:
            return Path("toto.yml")

        @pytest.fixture(scope="{scope}")
        def dsaf() -> None:
            pass

        def test_it(dsaf) -> None:
            pass
    """
    )
    result = pytester.runpytest()
    result.assert_outcomes(passed=1)


def test_e2e_postgres(pytester: pytest.Pytester) -> None:
    """Test that the postgres service is running."""
    pytester.makefile(".yml", docker_compose=DOCKER_COMPOSE)
    pytester.makepyfile(
        """
        from functools import partial
        import pytest
        from pytest_dsaf.utils import postgres_responsive

        @pytest.fixture(scope="session")
        def anyio_backend():
            return "asyncio"

        @pytest.fixture(scope="session")
        async def postgres_service(dsaf) -> None:
            await dsaf.start("db", check=partial(postgres_responsive, host="127.0.0.1",
            port=5555, user="postgres",
            password="example_password",
            database="postgres"))

        @pytest.mark.anyio
        async def test_it(postgres_service) -> None:
            pass
    """
    )
    result = pytester.runpytest()
    result.assert_outcomes(passed=1)
