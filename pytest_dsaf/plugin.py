"""Pytest plugin for Docker Service Registry."""

from __future__ import annotations

import asyncio
import os
import re
import subprocess
import timeit
from typing import TYPE_CHECKING, Any, Awaitable, Callable, Generator

import pytest

if TYPE_CHECKING:
    from pathlib import Path


__all__ = ["dsaf", "dsaf_compose_file", "dsaf_project_name", "dsaf_ip"]


def pytest_addoption(parser: pytest.Parser) -> None:
    group = parser.getgroup("dsaf", "dsaf plugin options")
    group.addoption(
        "--container-scope",
        type=str,
        action="store",
        default="session",
        help="The pytest fixture scope for reusing containers between tests.",
    )


def containers_scope(fixture_name: str, config: pytest.Config) -> str:  # noqa: ARG001
    return config.getoption("--container-scope", "session")


async def wait_until_responsive(
    check: Callable[..., Awaitable],
    timeout: float,
    pause: float,
    **kwargs: Any,
) -> None:
    """Wait until a service is responsive.

    Args:
    ----
        check: Coroutine, return truthy value when waiting should stop.
        timeout: Maximum seconds to wait.
        pause: Seconds to wait between calls to `check`.
        **kwargs: Given as kwargs to `check`.

    """
    ref = timeit.default_timer()
    now = ref
    while (now - ref) < timeout:
        if await check(**kwargs):
            return
        await asyncio.sleep(pause)
        now = timeit.default_timer()
    raise RuntimeError("Timeout reached while waiting on service!")


class DockerServiceRegistry:
    """A simple wrapper around docker compose to manage services."""

    def __init__(
        self,
        dsaf_base_command: list[str],
        dsaf_compose_file: Path,
        dsaf_project_name: str,
    ) -> None:
        """Initialize the DockerServiceRegistry and keep track of running services."""
        self._running_services: set[str] = set()
        self.docker_ip = self._get_docker_ip()
        self._base_command = [
            *dsaf_base_command,
            "-f",
            dsaf_compose_file.as_posix(),
            f"--project-name={dsaf_project_name}",
        ]

    @staticmethod
    def _get_docker_ip() -> str:
        docker_host = os.environ.get("DOCKER_HOST", "").strip()
        if not docker_host or docker_host.startswith("unix://"):
            return "127.0.0.1"
        if match := re.match(r"^tcp://(.+?):\d+$", docker_host):
            return match[1]
        raise ValueError(f'Invalid value for DOCKER_HOST: "{docker_host}".')

    def run_command(self, *args: str) -> None:
        """Run a docker compose command."""
        command = [*self._base_command, *args]
        try:
            subprocess.run(command, check=True, capture_output=True)  # noqa: S603
        except subprocess.CalledProcessError as e:
            raise RuntimeError(
                f"Command {e.cmd} failed with {e.returncode}: {e.stderr.decode()}"
            ) from e

    async def start(
        self,
        name: str,
        *,
        check: Callable[..., Awaitable],
        timeout: float = 30,
        pause: float = 0.1,
        **kwargs: Any,
    ) -> None:
        """Start a service and wait until it is responsive."""
        if name not in self._running_services:
            self.run_command("up", "-d", name)
            self._running_services.add(name)

            await wait_until_responsive(
                check=check,
                timeout=timeout,
                pause=pause,
                host=self.docker_ip,
                **kwargs,
            )

    def down(self) -> None:
        """Stop all running services."""
        self.run_command("down", "-t", "5")


@pytest.fixture(scope=containers_scope)
def dsaf_base_command() -> list[str]:
    """Docker Compose command to use.

    If you need to use old version of docker-compose,
    you can override this fixture in your test file return ["docker-compose"].
    """
    return ["docker", "compose"]


@pytest.fixture(scope=containers_scope)
def dsaf_compose_file(pytestconfig: Any) -> Path:
    """Fixture to set the path to the docker compose file."""
    return pytestconfig.rootpath / "docker_compose.yml"


@pytest.fixture(scope=containers_scope)
def dsaf_project_name() -> str:
    """Fixture to set the project name."""
    return "dsaf"


@pytest.fixture(scope=containers_scope)
def dsaf(
    dsaf_base_command: list[str], dsaf_compose_file: Path, dsaf_project_name: str
) -> Generator[DockerServiceRegistry, None, None]:
    """Fixture to manage docker services."""
    if not dsaf_compose_file.exists():
        raise pytest.UsageError(f"File not found: {dsaf_compose_file}")
    registry = DockerServiceRegistry(
        dsaf_base_command=dsaf_base_command,
        dsaf_compose_file=dsaf_compose_file,
        dsaf_project_name=dsaf_project_name,
    )
    try:
        yield registry
    finally:
        registry.down()


@pytest.fixture(scope=containers_scope)
def dsaf_ip(dsaf: DockerServiceRegistry) -> str:
    """Return the IP address of the docker daemon."""
    return dsaf.docker_ip
