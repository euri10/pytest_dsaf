"""Set of utility functions to be used as checks for the service fixture you create."""

import socket
from contextlib import closing


async def postgres_responsive(
    host: str, port: int, user: str, password: str, database: str
) -> bool:
    """Check if a postgres database is responsive."""
    import asyncpg

    try:
        conn = await asyncpg.connect(
            host=host,
            port=port,
            user=user,
            password=password,
            database=database,
        )
    except (ConnectionError, asyncpg.CannotConnectNowError):
        return False
    try:
        return await conn.fetchval("SELECT 1") == 1
    finally:
        await conn.close()


def check_socket(host: str, port: int) -> bool:
    """Check if a socket is open."""
    with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as sock:
        if sock.connect_ex((host, port)) == 0:
            return True
        return False
